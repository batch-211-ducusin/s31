// 1. What directive is used by Node.js in loading the modules it needs?
/*Sagot: require*/

// 2. What Node.js module contains a method for server creation?
/*Sagot: http module*/

// 3. What is the method of the http object responsible for creating a server using Node.js?
/*Sagot: createServer()*/

// 4. What method of the response object allows us to set status codes and content types?
/*Sagot: writeHead()*/

// 5. Where will console.log() output its contents when run in Node.js?
/*Sagot: terminal*/

// 6. What property of the request object contains the address's endpoint?
/*Sagot: URL*/