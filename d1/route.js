const http = require('http');

// Create a variable "port" to store the port number
const port = 4000;

// Create a varible "server" that stores the output of the "createServer" method.
const server = http.createServer((request, response) => {

	if(request.url == '/greeting'){
		response.writeHead(200, {'content-type': 'text/plain'});
		response.end('Hello Again');
	} else if(request.url == '/homepage'){
		response.writeHead(200, {'content-type': 'text/plain'});
		response.end('This is the homepage.');
	} else {
		response.writeHead(404, {'content-type': 'text/plain'});
		response.end('Page not available.');
	}

})

// Uses the "server" and "port" variable created above.
server.listen(port)

// When server is running, console will print the message in the terminal.
console.log(`Server now accessible at localhost: ${port}.`)

