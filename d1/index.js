//Node Introduction.
/*
Client-Server Architecture.

At this point, we know hot to:
	- Develop a static front-end application
	- Create and manipulate a MongoDB database

What if we need our front-end app to show dynamic contect? We'd need to send requests to a server that qould inturn query and manipulate the database for us.

Benefits of client-server
	- Centralized data makes applications more scalable and maintainable.
	- Multiple client apps may all use dynamically-generated data.
	- Workload is concetrated on the server, making client apps lightweight.
	- Improves data availability.

For JS developers, we have Node.js for building server-side application.


What is Node.js?
	- An open-source, javascript runtime environment for creating server-side application.

Runtime Environment.
	- Gives the context for rinnung a programming language.
	- JS was initially within the context of the browser, giving it access to:
		* DOM
		* window object
		* local storage

Benifits of Node.js
	- Performance
		* Optimized for web applications
	- Familiarity
		* "Same Old" Javascript
	- Access to Node Package Manager (NPM)
		* World's largest registry of packages.
*/



//Code-Along
/*
	npm documentation: https://www.npmjs.com/
	
	Request Object: https://developer.mozilla.org/en-US/docs/Web/API/Request
	
	Response Object: https://developer.mozilla.org/en-US/docs/Web/API/Response

	HTTP Response Status Codes: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
*/
/*
- Use the "require" directive to load Node.js modules

- A "module" is a software component or part of a program that contains one or more routines.

- The "http module" lets Node.js tranfer data using the Hyper Text Transfer Protocol.

- The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications.

- HTTP is a protocol that allow the fetching of resources such as HTML documents.

- Clients (browser) and servers (Node.js or Express.js applications) communicate by exchanging individual messages.

- The messages sent by the client, usually, a Web Browser, are called requests.

- The messages sent by the server as an answer are called responses.
*/
let http = require("http");


/*
- The http module has a createServer method that accepts a function as an argument and allows for a creation of a server.

- The arguments passed in the function are request and response objects that contains method that allow us to receive requests from the client and send responses back to it.
*/
http.createServer(function (request, response) {

	/*
		Use the writeHead() method to:
		- Set a status code for the response- a 200 means OK.
		- Set the content-type of the response as a plain text message
	*/
	response.writeHead(200, {'content-Type': 'text/plain'})
	
	/*
		Send the reponse with a text content "Hello World"
	*/
	response.end('Hello World');

}).listen(4000)

/*
- A port is a virtual point where network connections start and end.

- Each port is associated with a specific process or service

- The server will be assigned to port 4000 via the "listed(4000)" method where the server will listen to any requests that are sent to it, eventually communicating with our server.
*/

console.log('Server running at localhost:4000');